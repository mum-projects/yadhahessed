import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-money-input',
  templateUrl: './money-input.component.html',
  styleUrls: ['./money-input.component.scss']
})
export class MoneyInputComponent implements OnInit {
  money = new FormControl('');
  constructor() {
  }

  ngOnInit(): void {
  }

}
