<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DonationController extends Controller
{
    public function donation(Request $request)
    {
        if ($request->isMethod('post')) {
            Validator::make($request->all(), [
                'name' => 'required',
                'nickname' => 'required',
                'phone' => 'required|numeric',
                'email' => 'email',
                'address' => 'required',
                'zipCode' => 'required',
                'city' => 'required',
                'money' => 'required|numeric'
            ])->validate();

            return view('donation', ['success' => true]);
        } else {
            return view('donation', ['success' => false]);
        }
    }

}
