@extends('layouts.base')

<script src="https://js.stripe.com/v3/"></script>

@section('content')
    <div id="donation">
        <div id="logo" class="center-align">
            <img alt="logo Yadhahessed" src="{{ asset('images/yadhahessed-logo.png') }}">
        </div>
        <br>
        <div class="container">
            @if($errors->any())
                <script>
                    M.toast({html: 'Tous les champs sont obligatoires'});
                </script>
            @endif
            <div class="row">
                <form class="col s12" method="post" action="{{ route('donation') }}">
                    @csrf
                    <div class="row">
                        <div class="input-field col l3 m6 s12">
                            <input type="text"
                                   id="name"
                                   name="name"
                                   value="{{ old('name') }}"
                                   autofocus
                                   required>
                            <label for="name">Nom</label>
                        </div>
                        <div class="input-field col l3 m6 s12">
                            <input type="text"
                                   id="nickname"
                                   name="nickname"
                                   value="{{ old('nickname') }}"
                                   required>
                            <label for="nickname">Prénom</label>
                        </div>
                        <div class="input-field col l3 m6 s12">
                            <input type="tel"
                                   id="phone"
                                   name="phone"
                                   value="{{ old('phone') }}"
                                   required>
                            <label for="phone">Numéro de téléphone</label>
                        </div>
                        <div class="input-field col l3 m6 s12">
                            <input type="email"
                                   id="email"
                                   name="email"
                                   value="{{ old('email') }}"
                                   required>
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col m4 s12">
                            <input type="text"
                                   id="address"
                                   name="address"
                                   value="{{ old('address') }}"
                                   required>
                            <label for="address">Adresse</label>
                        </div>
                        <div class="input-field col m4 s12">
                            <input type="text"
                                   id="zipCode"
                                   name="zipCode"
                                   value="{{ old('zipCode') }}"
                                   required>
                            <label for="zipCode">Code postal</label>
                        </div>
                        <div class="input-field col m4 s12">
                            <input type="text"
                                   id="city"
                                   name="city"
                                   value="{{ old('city') }}"
                                   required>
                            <label for="city">Ville</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col l5 m5 s12">
                            <i class="material-icons prefix">euro_symbol</i>
                            <input type="number"
                                   id="money"
                                   name="money"
                                   value="{{ old('money') }}"
                                   autocomplete="off"
                                   required>
                            <label for="money">Montant du don</label>
                        </div>
                        <div class="input-field col l7 m7 s12">
                            <div id="card-element"><!--Stripe.js injects the Card Element--></div>
                        </div>
                    </div>
                    <div class="right-align">
                        <button class="btn waves-effect waves-light"
                                type="button"
                                name="action">Faire un don
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/donation/donation.js') }}"></script>
@endpush

