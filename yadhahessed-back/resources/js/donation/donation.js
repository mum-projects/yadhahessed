M.AutoInit();


// Stripe
const stripe = Stripe("pk_test_51H5QxJACBnBkjLilQOBMtdngas9SQeby1IzXZ0KSw0YY1Kq0YPeJcvp47nejXC41EJqArXhr3QAh9l4v7Bff9GNZ00fvJJz52M");
const elements = stripe.elements();
const style = {
    base: {
        color: "#32325d",
        fontFamily: 'Arial, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
            color: "#32325d"
        }
    },
    invalid: {
        fontFamily: 'Arial, sans-serif',
        color: "#fa755a",
        iconColor: "#fa755a"
    }
};
const card = elements.create("card", {style: style});
card.mount("#card-element");
