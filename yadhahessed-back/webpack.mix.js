const mix = require('laravel-mix');
const hmrPort = 8090

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/donation/donation.js', 'public/js/donation')
    .sass('resources/sass/app.scss', 'public/css')
    .copyDirectory('resources/images', 'public/images');


mix.options({
    hmrOptions: {
        host: 'localhost',
        port: hmrPort
    },
});

mix.webpackConfig({
    mode: 'development',
    devServer: {
        port: hmrPort
    },
});
